package org.shoply;

import lombok.RequiredArgsConstructor;
import org.shoply.cli.AppManagerCommandLine;
import org.shoply.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import java.util.Optional;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class DemoCommandLineRunner implements CommandLineRunner {
    private final AppManagerCommandLine appManagerCommandLine;

    @Override
    public void run(String... args) throws Exception {
        this.appManagerCommandLine.manage();
        System.out.println("FINISH");
    }

}

