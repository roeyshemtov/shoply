package org.shoply.repository;

import org.shoply.model.Purchase;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PurchaseRepository extends CrudRepository<Purchase, String> {
    public List<Purchase> findPurchasesByUserId(Long id);
}
