package org.shoply.repository;

import org.shoply.model.Product;
import org.shoply.model.ProductStock;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductStockRepository extends CrudRepository<ProductStock, Long> {
    public List<ProductStock> findAll();

    public ProductStock findProductStockByProduct(Product product);

}
