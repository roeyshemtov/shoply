package org.shoply.model;

import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
public class Customer extends User {
    public Customer(String username, String password, String email, String address, String creditCardNumber) {
        super(username, password, email, address, creditCardNumber, false);
    }

}
