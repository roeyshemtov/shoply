package org.shoply.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@Table(name = "purchases")
public class Purchase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long purchaseId;
    private Long userId;
    private Double purchaseAmount;
    private LocalDateTime purchaseDate;

    public Purchase(Long userId, Double purchaseAmount) {
        this.userId = userId;
        this.purchaseAmount = purchaseAmount;
        this.purchaseDate = LocalDateTime.now();
    }

    public String toString() {
        return "Purchase number: " + purchaseId + " In: " + purchaseDate.toString() + " was cost: " + purchaseAmount + " ILS";
    }

}
