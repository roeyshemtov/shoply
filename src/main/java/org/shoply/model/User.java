package org.shoply.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Data
@NoArgsConstructor
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    @NotNull(message = "Username cannot be null")
    @Size(min = 4, max = 10, message = "User must be between 4 to 10 characters")
    private String username;
    @NotNull(message = "Password cannot be null")
    @Size(min = 4, max = 10, message = "Password must be between 4 to 10 characters")
    private String password;
    @Column(unique = true)
    @Pattern(regexp = ".+@.+\\..+", message = "Please provide a valid email address")
    @NotNull(message = "Email cannot be null")
    private String email;
    private String address;
    @NotNull(message = "Credit Card cannot be null")
    @Pattern(regexp = "^(\\d{12}|\\d{16})$",
            message = "credit card number should be 12 or 16 digits")
    private String creditCardNumber;
    private boolean isAdmin;

    public User(String username, String password, String email, String address, String creditCardNumber, boolean isAdmin) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.address = address;
        this.creditCardNumber = creditCardNumber;
        this.isAdmin = isAdmin;
    }
}
