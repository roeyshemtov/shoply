package org.shoply.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
public class Cart {
    private Map<Product, Integer> products = new HashMap<>();

    public Double getTotalCartPrice() {
        return products
                .entrySet()
                .stream()
                .mapToDouble(e -> e.getKey().getPrice() * e.getValue())
                .sum();
    }

    public boolean isEmpty() {
        return this.products.isEmpty();
    }

    public void printCart() {
        if (this.products.isEmpty()) {
            System.out.println("Your cart is empty");
            return;
        }
        System.out.println("Total cart price: " + this.getTotalCartPrice() + " ILS");
        System.out.println("Cart details:");
        this.products.forEach((p, a) -> {
            System.out.println(a + "X" + p.getProductName() + " --- " + a * p.getPrice() + " ILS");
        });
    }

}
