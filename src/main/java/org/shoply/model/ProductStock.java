package org.shoply.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table(name = "products_stock")
public class ProductStock {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToOne
    @JoinColumn(name = "productId")
    private Product product;
    private Integer amount;

    public String toString() {
        return this.product.toString() + " , Available amount: " + this.amount;
    }
}
