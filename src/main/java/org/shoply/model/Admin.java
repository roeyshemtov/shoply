package org.shoply.model;

import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
public class Admin extends User {

    public Admin(String username, String password, String email, String address, String creditCardNumber) {
        super(username, password, email, address, creditCardNumber,true);
    }

}
