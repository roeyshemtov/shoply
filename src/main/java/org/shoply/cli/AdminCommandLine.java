package org.shoply.cli;

import lombok.RequiredArgsConstructor;
import org.shoply.model.Admin;
import org.shoply.model.User;
import org.shoply.repository.PurchaseRepository;
import org.shoply.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AdminCommandLine {

    private final PurchaseRepository purchaseRepository;
    private final UserRepository userRepository;

    public void manage(Admin adminUser) {
        System.out.println("Welcome " + adminUser.getUsername() + " to shoply!");

        while (true) {
            System.out.println("===========================");
            System.out.println("What Will you want to do?");
            System.out.println("===========================");

            int choice = this.choose();
            switch (choice) {
                case 1:
                    this.printAllCustomersPurchases();
                    break;
                case 0:
                    System.exit(0);
            }
        }


    }

    private void printAllCustomersPurchases() {
        this.purchaseRepository.findAll().forEach(purchase -> {
            Long userId = purchase.getUserId();
            User user = this.userRepository.findById(userId).get();
            System.out.println(purchase.toString() + " By " + user.getUsername());
        });

    }

    private int choose() {
        System.out.println("1.Show all purchases");
        System.out.println("0.Exit");

        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

}