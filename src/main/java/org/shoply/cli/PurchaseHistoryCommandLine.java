package org.shoply.cli;

import lombok.RequiredArgsConstructor;
import org.shoply.repository.PurchaseRepository;
import org.shoply.model.Purchase;
import org.shoply.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PurchaseHistoryCommandLine {
    private final PurchaseRepository purchaseRepository;

    public void printHistory(User user) {
        List<Purchase> purchases = this.purchaseRepository.findPurchasesByUserId(user.getId());
        if (purchases.isEmpty()) {
            System.out.println(user.getUsername() +", You don't have any purchases history");
        } else {
            purchases.forEach(System.out::println);
        }
    }
}
