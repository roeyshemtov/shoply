package org.shoply.cli;

import lombok.RequiredArgsConstructor;
import org.shoply.model.Customer;
import org.shoply.repository.UserRepository;
import org.shoply.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RegistrationCommandLine {
    private final UserRepository userRepository;

    public Optional<User> register() {
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Welcome to Shoply Registration:\n");
            System.out.println("Username:");
            String username = scanner.nextLine();
            System.out.println("Password:");
            String password = scanner.nextLine();
            System.out.println("Email:");
            String email = scanner.nextLine();
            System.out.println("Adress:");
            String address = scanner.nextLine();
            System.out.println("Credit Card Number:");
            String creditCardNumber = scanner.nextLine();

            User user = new Customer(username, password, email, address, creditCardNumber);
            if (!this.isUserValid(user)) {
                return Optional.empty();
            }
            userRepository.save(user);
            System.out.println(username + " Register succesfully");
            return Optional.of(user);
        } catch (Exception e) {
            System.out.println("Failed to register - probably user is taken");
            return Optional.empty();
        }
    }

    private boolean isUserValid(User user) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        if (violations.isEmpty()) {
            return true;
        }
        violations.forEach(violation -> System.out.println(violation.getMessage()));
        return false;
    }

}
