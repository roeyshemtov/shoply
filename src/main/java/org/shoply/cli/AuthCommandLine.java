package org.shoply.cli;

import lombok.RequiredArgsConstructor;
import org.shoply.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Scanner;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuthCommandLine {
    private final RegistrationCommandLine registrationCommandLine;
    private final LoginCommandLine loginCommandLine;

    public Optional<User> auth() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please Register or Login");
        while (true) {
            try {
                System.out.println("1.Register");
                System.out.println("2.Login");
                int choose = scanner.nextInt();
                if (choose != 1 && choose != 2) {
                    System.out.println("Please press 1 or 2");
                    Thread.sleep(500);
                    continue;
                }
                switch (choose) {
                    case 1:
                        return this.registrationCommandLine.register();
                    case 2:
                        return this.loginCommandLine.login();
                }
            } catch (Exception e) {
                //TODO: Write log
                System.out.println(e.getMessage());
            }
        }


    }
}
