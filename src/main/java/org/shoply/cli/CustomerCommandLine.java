package org.shoply.cli;

import lombok.RequiredArgsConstructor;
import org.shoply.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CustomerCommandLine {
    private final AuthCommandLine authCommandLine;
    private final PurchaseCommandLine purchaseCommandLine;
    private final PurchaseHistoryCommandLine purchaseHistoryCommandLine;

    public void manage(Customer customerUser) {
        System.out.println("Welcome " + customerUser.getUsername() + " to shoply!");
        while (true) {
            System.out.println("===========================");
            System.out.println("What Will you want to do?");
            System.out.println("===========================");

            int choice = this.choose(customerUser);
            switch (choice) {
                case 1:
                    this.purchaseCommandLine.purchase(customerUser);
                    break;
                case 2:
                    this.purchaseHistoryCommandLine.printHistory(customerUser);
                    break;
                case 0:
                    System.exit(0);
            }
        }

    }

    private int choose(Customer user) {
        System.out.println("1.Buy Items");
        System.out.println("2.Show last purchases");
        System.out.println("0.Exit");

        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

}
