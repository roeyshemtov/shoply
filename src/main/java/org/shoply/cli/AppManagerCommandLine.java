package org.shoply.cli;

import lombok.RequiredArgsConstructor;
import org.shoply.model.Admin;
import org.shoply.model.Customer;
import org.shoply.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AppManagerCommandLine {
    private final AuthCommandLine authCommandLine;
    private final CustomerCommandLine customerCommandLine;
    private final AdminCommandLine adminCommandLine;

    public void manage() {
        Optional<User> userOpt = Optional.empty();
        while (!userOpt.isPresent()) {
            userOpt = this.authCommandLine.auth();
        }
        User user = userOpt.get();
        if (user.isAdmin()) {
            this.adminCommandLine.manage((Admin) user);
        } else {
            this.customerCommandLine.manage((Customer) user);
        }

    }

}
