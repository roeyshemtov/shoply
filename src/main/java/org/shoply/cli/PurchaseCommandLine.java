package org.shoply.cli;

import lombok.RequiredArgsConstructor;
import org.shoply.model.*;
import org.shoply.repository.ProductStockRepository;
import org.shoply.repository.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PurchaseCommandLine {

    private final ProductStockRepository productStockRepository;
    private final PurchaseRepository purchaseRepository;

    public void purchase(User user) {
        Cart cart = new Cart();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println();
            System.out.println();
            System.out.println("=====Choose your option=====:");
            System.out.println("1.See available stock");
            System.out.println("2.Add product to cart");
            System.out.println("3.Show your cart");
            System.out.println("4.Purchase cart");
            System.out.println("5.Truncate cart");
            System.out.println("0.Back");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    this.printStock();
                    break;
                case 2:
                    this.addItemToCart(cart);
                    break;
                case 3:
                    this.printCart(cart);
                    break;
                case 4:
                    this.purchaseCart(user, cart);
                case 5:
                    this.truncateCart(cart);
                    break;
                case 0:
                    return;
            }
        }
    }

    private void purchaseCart(User user, Cart cart) {
        if (cart.isEmpty()) {
            System.out.println("Your cart is empty");
            return;
        }
        Purchase purchase = new Purchase(user.getId(), cart.getTotalCartPrice());
        this.purchaseRepository.save(purchase);
        cart.getProducts().forEach((p, a) -> {
            ProductStock ps = this.productStockRepository.findProductStockByProduct(p);
            ps.setAmount(ps.getAmount() - a);
            this.productStockRepository.save(ps);
        });
        System.out.println("Purchase cart successfully");
        System.out.println(user.getUsername() + ", Thanks for buying in Shoply!");
        System.out.println("Your reception:");
        System.out.println("PurchaseID: " + purchase.getPurchaseId());
        cart.printCart();
    }

    private void truncateCart(Cart cart) {
        cart.getProducts().clear();
    }

    private void printStock() {
        List<ProductStock> stock = this.productStockRepository.findAll();
        if (stock.isEmpty()) {
            System.out.println("There is no available stock");
            return;
        }
        System.out.println("Currently available stock:");
        stock.forEach(System.out::println);
    }

    private void printCart(Cart cart) {
        cart.printCart();
    }

    private void addItemToCart(Cart cart) {
        List<ProductStock> stock = this.productStockRepository.findAll();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter product id:");
        int productId = scanner.nextInt();
        Optional<ProductStock> selectedProduct = stock.stream()
                .filter(productStock -> productStock.getProduct().getProductId() == productId).findAny();
        if (!selectedProduct.isPresent()) {
            System.out.println("Sorry but productId " + productId + " is not exists");
            return;
        }
        System.out.println("Enter amount:");
        int amount = scanner.nextInt();
        int previousAmount = cart.getProducts().getOrDefault(selectedProduct.get().getProduct(), 0);

        if (previousAmount + amount > selectedProduct.get().getAmount()) {
            System.out.println("Your order is more than stock available");
            return;
        }
        cart.getProducts().put(selectedProduct.get().getProduct(), previousAmount + amount);
    }

}
