package org.shoply.cli;

import lombok.RequiredArgsConstructor;
import org.shoply.repository.UserRepository;
import org.shoply.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Scanner;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LoginCommandLine {
    private final UserRepository userRepository;

    public Optional<User> login() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please Login");
        System.out.println("Enter Username:");
        String username = scanner.next();
        System.out.println("Enter Password:");
        String password = scanner.next();
        Optional<User> user = this.userRepository.findRegisteredUserByUsername(username);
        if (!user.isPresent()) {
            System.out.println("User " + username + " didn't appear on the system.");
            return Optional.empty();
        }
        if (!user.get().getPassword().equals(password)) {
            System.out.println("Password is not correct");
            return Optional.empty();
        }
        System.out.println("Logged in successfully");

        return user;
    }

}
