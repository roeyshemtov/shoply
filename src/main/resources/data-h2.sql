/* Insert Users */
INSERT INTO users (DTYPE,username, password,email,address,credit_card_number, is_admin)
SELECT 'Admin','admin', 'admin','example@gmail.com','Holon','1234123412341234', TRUE
WHERE NOT EXISTS (SELECT * FROM users WHERE username='admin');

/*Insert Products*/
INSERT INTO products (product_name,price)
SELECT 'tomato',5.0
WHERE NOT EXISTS (SELECT * FROM products WHERE product_name='tomato');

INSERT INTO products (product_name,price)
SELECT 'tuna',3.5
WHERE NOT EXISTS (SELECT * FROM products WHERE product_name='tuna');

INSERT INTO products (product_name,price)
SELECT 'watermelon',20.0
WHERE NOT EXISTS (SELECT * FROM products WHERE product_name='watermelon');

INSERT INTO products (product_name,price)
SELECT 'melon',10.0
WHERE NOT EXISTS (SELECT * FROM products WHERE product_name='melon');

INSERT INTO products (product_name,price)
SELECT 'apple',4.0
WHERE NOT EXISTS (SELECT * FROM products WHERE product_name='apple');

INSERT INTO products (product_name,price)
SELECT 'milk',8.0
WHERE NOT EXISTS (SELECT * FROM products WHERE product_name='milk');

INSERT INTO products (product_name,price)
SELECT 'bread',8.0
WHERE NOT EXISTS (SELECT * FROM products WHERE product_name='bread');

INSERT INTO products (product_name,price)
SELECT 'cola zero',7.0
WHERE NOT EXISTS (SELECT * FROM products WHERE product_name='cola zero');

INSERT INTO products (product_name,price)
SELECT 'ice cream',5.0
WHERE NOT EXISTS (SELECT * FROM products WHERE product_name='ice cream');

INSERT INTO products (product_name,price)
SELECT 'frozen pizza',25.0
WHERE NOT EXISTS (SELECT * FROM products WHERE product_name='frozen pizza');

/*Insert Stock*/
INSERT INTO products_stock (product_id,amount)
SELECT 1,5
WHERE NOT EXISTS (SELECT * FROM products_stock WHERE product_id=1);

INSERT INTO products_stock (product_id,amount)
SELECT 2,20
WHERE NOT EXISTS (SELECT * FROM products_stock WHERE product_id=2);

INSERT INTO products_stock (product_id,amount)
SELECT 3,15
WHERE NOT EXISTS (SELECT * FROM products_stock WHERE product_id=3);

INSERT INTO products_stock (product_id,amount)
SELECT 4,4
WHERE NOT EXISTS (SELECT * FROM products_stock WHERE product_id=4);

INSERT INTO products_stock (product_id,amount)
SELECT 5,9
WHERE NOT EXISTS (SELECT * FROM products_stock WHERE product_id=5);

INSERT INTO products_stock (product_id,amount)
SELECT 6,10
WHERE NOT EXISTS (SELECT * FROM products_stock WHERE product_id=6);

INSERT INTO products_stock (product_id,amount)
SELECT 7,20
WHERE NOT EXISTS (SELECT * FROM products_stock WHERE product_id=7);

INSERT INTO products_stock (product_id,amount)
SELECT 8,21
WHERE NOT EXISTS (SELECT * FROM products_stock WHERE product_id=8);

INSERT INTO products_stock (product_id,amount)
SELECT 9,12
WHERE NOT EXISTS (SELECT * FROM products_stock WHERE product_id=9);

INSERT INTO products_stock (product_id,amount)
SELECT 10,5
WHERE NOT EXISTS (SELECT * FROM products_stock WHERE product_id=10);
