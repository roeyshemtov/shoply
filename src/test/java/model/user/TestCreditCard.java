package model.user;

import org.junit.jupiter.api.Test;
import org.shoply.model.Customer;
import org.shoply.model.User;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCreditCard extends UserTest {

    @Test
    public void creditCardNotNull() {
        User user = new Customer(validUsername, validPassword, validEmail, validAddress, null);
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 1);
        ConstraintViolation<User> usernameConstraintViolation = violations.iterator().next();
        assertEquals(usernameConstraintViolation.getMessage(), "Credit Card cannot be null");
    }

    @Test
    public void creditCardUnderLength() {
        User user = new Customer(validUsername, validPassword, validEmail, validAddress, "1234");
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 1);
        ConstraintViolation<User> usernameConstraintViolation = violations.iterator().next();
        assertEquals(usernameConstraintViolation.getMessage(), "credit card number should be 12 or 16 digits");
    }

    @Test
    public void creditCardAboveLength() {
        User user = new Customer(validUsername, validPassword, validEmail, validAddress, "12345123451234512345");
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 1);
        ConstraintViolation<User> usernameConstraintViolation = violations.iterator().next();
        assertEquals(usernameConstraintViolation.getMessage(), "credit card number should be 12 or 16 digits");
    }

    @Test
    public void creditCardNotOnlyDigits() {
        User user = new Customer(validUsername, validPassword, validEmail, validAddress, "1234512345ab");
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 1);
        ConstraintViolation<User> usernameConstraintViolation = violations.iterator().next();
        assertEquals(usernameConstraintViolation.getMessage(), "credit card number should be 12 or 16 digits");
    }

    @Test
    public void validCreditCard12Digits() {
        User user = new Customer(validUsername, validPassword, validEmail, validAddress, "123451234512");
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 0);
    }

    @Test
    public void validCreditCard16Digits() {
        User user = new Customer(validUsername, validPassword, validEmail, validAddress, "1234512345123451");
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 0);
    }


}
