package model.user;

import org.junit.jupiter.api.Test;
import org.shoply.model.Customer;
import org.shoply.model.User;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestUserName extends UserTest {

    @Test
    public void usernameNotNull() {
        User user = new Customer(null, validPassword, validEmail, validAddress, validCreditCard);
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 1);
        ConstraintViolation<User> usernameConstraintViolation = violations.iterator().next();
        assertEquals(usernameConstraintViolation.getMessage(), "Username cannot be null");
    }

    @Test
    public void usernameUnderMinimumCharacters() {
        User user = new Customer("1", validPassword, validEmail, validAddress, validCreditCard);
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 1);
        ConstraintViolation<User> usernameConstraintViolation = violations.iterator().next();
        assertEquals(usernameConstraintViolation.getMessage(), "User must be between 4 to 10 characters");
    }

    @Test
    public void usernameAboveMaximumCharacters() {
        User user = new Customer("100100100100", validPassword, validEmail, validAddress, validCreditCard);
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 1);
        ConstraintViolation<User> usernameConstraintViolation = violations.iterator().next();
        assertEquals(usernameConstraintViolation.getMessage(), "User must be between 4 to 10 characters");
    }

    @Test
    public void usernameWithCorrectCharacters() {
        User user = new Customer("username", validPassword, validEmail, validAddress, validCreditCard);
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 0);
    }


}
