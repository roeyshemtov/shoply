package model.user;

import org.junit.jupiter.api.Test;
import org.shoply.model.Customer;
import org.shoply.model.User;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestPassword extends UserTest {

    @Test
    public void usernameNotNull() {
        User user = new Customer(validUsername, null, validEmail, validAddress, validCreditCard);
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 1);
        ConstraintViolation<User> usernameConstraintViolation = violations.iterator().next();
        assertEquals(usernameConstraintViolation.getMessage(), "Password cannot be null");
    }

    @Test
    public void passwordUnderMinimumCharacters() {
        User user = new Customer(validUsername, "1", validEmail, validAddress, validCreditCard);
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 1);
        ConstraintViolation<User> usernameConstraintViolation = violations.iterator().next();
        assertEquals(usernameConstraintViolation.getMessage(), "Password must be between 4 to 10 characters");
    }

    @Test
    public void passwordAboveMaximumCharacters() {
        User user = new Customer(validUsername, "100100100100", validEmail, validAddress, validCreditCard);
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 1);
        ConstraintViolation<User> usernameConstraintViolation = violations.iterator().next();
        assertEquals(usernameConstraintViolation.getMessage(), "Password must be between 4 to 10 characters");
    }

    @Test
    public void passwordWithCorrectCharacters() {
        User user = new Customer(validUsername, "validpass", validEmail, validAddress, validCreditCard);
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 0);
    }

}
