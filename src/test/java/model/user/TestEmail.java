package model.user;

import org.junit.jupiter.api.Test;
import org.shoply.model.Customer;
import org.shoply.model.User;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestEmail extends UserTest {

    @Test
    public void emailNotNull() {
        User user = new Customer(validUsername, validPassword, null, validAddress, validCreditCard);
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 1);
        ConstraintViolation<User> usernameConstraintViolation = violations.iterator().next();
        assertEquals(usernameConstraintViolation.getMessage(), "Email cannot be null");
    }

    @Test
    public void invalidEmailWithoutAtSign() {
        User user = new Customer(validUsername, validPassword, "invalidemail.com", validAddress, validCreditCard);
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 1);
        ConstraintViolation<User> usernameConstraintViolation = violations.iterator().next();
        assertEquals(usernameConstraintViolation.getMessage(), "Please provide a valid email address");
    }

    @Test
    public void invalidEmailWithoutDot() {
        User user = new Customer(validUsername, validPassword, "invalidemail@gmail", validAddress, validCreditCard);
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 1);
        ConstraintViolation<User> usernameConstraintViolation = violations.iterator().next();
        assertEquals(usernameConstraintViolation.getMessage(), "Please provide a valid email address");
    }

    @Test
    public void validEmail() {
        User user = new Customer(validUsername, validPassword, "validemail@gmail.com", validAddress, validCreditCard);
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(violations.size(), 0);
    }
}
