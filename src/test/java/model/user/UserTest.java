package model.user;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public abstract class UserTest {

    protected Validator validator;

    protected String validUsername = "validuser";
    protected String validPassword = "validpw";
    protected String validEmail = "validemail@gmail.com";
    protected String validAddress = "Holon";
    protected String validCreditCard = "1111222233334444";

    public UserTest() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        this.validator = factory.getValidator();
    }


}
